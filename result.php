<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style.css">
    <script type="text/javascript">

    </script>

    <title>Resultados! :)</title>
</head>

<?php
include "./controllers/resultController.php";

$resultController = new resultController();

?>

<body>
    <div class="card-main" style="grid-column: 2/3;">
        <div class="card-title">
            <p class="title">Resultados</p>
        </div>
        <div class="block-radio">
            <div class="question">
                <div class="header">
                    <? print_r($resultController->controlador->getQuestionById($resultController->Q_radio,$resultController->evaluacion[0]['id_question'])) ?>
                </div>
                <div class="bodyr">
                    <?if($resultController->evaluacion[0]['status']==='true'){?>
                    <div class="wrapper-success">
                        <input class="" type="radio" checked disabled>
                        <label class="label-success" for="1">
                            <span class="text">
                                <? print_r($resultController->controlador->getAnswerById($resultController->Q_radio,$resultController->evaluacion[0]['id_question'],$resultController->evaluacion[0]['id_selected'])) ?>
                            </span>
                        </label>
                    </div>
                    <?}else{?>
                    <div class="wrapper-danger">
                        <input class="" type="radio" checked disabled>
                        <label class="label-danger" for="1">
                            <span class="text">
                                <? print_r($resultController->controlador->getAnswerById($resultController->Q_radio,$resultController->evaluacion[0]['id_question'],$resultController->evaluacion[0]['id_selected'])) ?>
                            </span>
                        </label>
                    </div>
                    <?}?>
                </div>
                <div class="footer">
                    <?if($resultController->evaluacion[0]['status'] !== 'true'){?>
                    <span>La respuesta correcta es: <span style="color:#76d275;font-weight: bold;">
                            <?print_r($resultController->controlador->getAnswerById($resultController->Q_radio,$resultController->evaluacion[0]['id_question'], $resultController->getAnswer($resultController->Q_radio,$resultController->evaluacion[0]['id_question'],'Radio')))?>
                        </span></span>
                    <?}else{?>
                    <span>Puntos: 3pts.</span>
                    <?}?>
                </div>
            </div>
            <div class="question">
                <div class="header">
                    <? print_r($resultController->controlador->getQuestionById($resultController->Q_radio,$resultController->evaluacion[1]['id_question'])) ?>
                </div>
                <div class="bodyr">
                    <?if($resultController->evaluacion[1]['status']==='true'){?>
                    <div class="wrapper-success">
                        <input class="" type="radio" checked disabled>
                        <label class="label-success" for="1">
                            <span class="text">
                                <? print_r($resultController->controlador->getAnswerById($resultController->Q_radio,$resultController->evaluacion[1]['id_question'],$resultController->evaluacion[1]['id_selected'])) ?>
                            </span>
                        </label>
                    </div>
                    <?}else{?>
                    <div class="wrapper-danger">
                        <input class="" type="radio" checked disabled>
                        <label class="label-danger" for="1">
                            <span class="text">
                                <? print_r($resultController->controlador->getAnswerById($resultController->Q_radio,$resultController->evaluacion[1]['id_question'],$resultController->evaluacion[1]['id_selected'])) ?>
                            </span>
                        </label>
                    </div>
                    <?}?>
                </div>
                <div class="footer">
                    <?if($resultController->evaluacion[1]['status'] !== 'true'){?>
                    <span>La respuesta correcta es: <span style="color:#76d275;font-weight: bold;">
                            <?print_r($resultController->controlador->getAnswerById($resultController->Q_radio,$resultController->evaluacion[1]['id_question'], $resultController->getAnswer($resultController->Q_radio,$resultController->evaluacion[1]['id_question'],'Radio')))?>
                        </span></span>
                    <?}else{?>
                    <span>Puntos: 3pts.</span>
                    <?}?>
                </div>
            </div>
        </div>

        <div class="block-select">
            <div class="questions">
                <div class="headers">
                    <? print_r($resultController->controlador->getQuestionById($resultController->Q_select,$resultController->evaluacion[2]['id_question'])) ?>
                </div>
                <div class="bodys">
                    <?if($resultController->evaluacion[2]['status']==='true'){?>
                    <div class="wrapper-success">
                        <select selected class="select-css-success" disabled>
                            <option>
                                <? print_r($resultController->controlador->getAnswerById($resultController->Q_select,$resultController->evaluacion[2]['id_question'],$resultController->evaluacion[2]['id_selected'])) ?>
                            </option>
                        </select>
                    </div>
                    <?}else{?>
                    <div class="wrapper-danger">
                        <select selected class="select-css-danger" disabled>
                            <option>
                                <? print_r($resultController->controlador->getAnswerById($resultController->Q_select,$resultController->evaluacion[2]['id_question'],$resultController->evaluacion[2]['id_selected'])) ?>
                            </option>
                        </select>
                    </div>
                    <?}?>

                </div>
                <div class="footers">
                    <?if($resultController->evaluacion[2]['status'] !== 'true'){?>
                    <span>La respuesta correcta es: <span style="color:#76d275;font-weight: bold;">
                            <?print_r($resultController->controlador->getAnswerById($resultController->Q_select,$resultController->evaluacion[2]['id_question'], $resultController->getAnswer($resultController->Q_select,$resultController->evaluacion[2]['id_question'],'Select')))?>
                        </span></span>
                    <?}else{?>
                    <span>Puntos: 3pts.</span>
                    <?}?>
                </div>
            </div>
            <div class="questions">
                <div class="headers">
                    <? print_r($resultController->controlador->getQuestionById($resultController->Q_select,$resultController->evaluacion[3]['id_question'])) ?>
                </div>
                <div class="bodys">
                    <?if($resultController->evaluacion[3]['status']==='true'){?>
                    <div class="wrapper-success">
                        <select selected class="select-css-success" disabled>
                            <option>
                                <? print_r($resultController->controlador->getAnswerById($resultController->Q_select,$resultController->evaluacion[3]['id_question'],$resultController->evaluacion[3]['id_selected'])) ?>
                            </option>
                        </select>
                    </div>
                    <?}else{?>
                    <div class="wrapper-danger">
                        <select selected class="select-css-danger" disabled>
                            <option>
                                <? print_r($resultController->controlador->getAnswerById($resultController->Q_select,$resultController->evaluacion[3]['id_question'],$resultController->evaluacion[3]['id_selected'])) ?>
                            </option>
                        </select>
                    </div>
                    <?}?>

                </div>
                <div class="footers">
                    <?if($resultController->evaluacion[3]['status'] !== 'true'){?>
                    <span>La respuesta correcta es: <span style="color:#76d275;font-weight: bold;">
                            <?print_r($resultController->controlador->getAnswerById($resultController->Q_select,$resultController->evaluacion[3]['id_question'], $resultController->getAnswer($resultController->Q_select,$resultController->evaluacion[3]['id_question'],'Select')))?>
                        </span></span>
                    <?}else{?>
                    <span>Puntos: 3pts.</span>
                    <?}?>
                </div>
            </div>
        </div>
        <div class="block-select">
            <div class="questions">
                <div class="headers">
                    <? print_r($resultController->controlador->getQuestionById($resultController->Q_select,$resultController->evaluacion[2]['id_question'])) ?>
                </div>
                <div class="bodys">
                    <?if($resultController->evaluacion[2]['status']==='true'){?>
                    <div class="wrapper-success">
                        <select selected class="select-css-success" disabled>
                            <option>
                                <? print_r($resultController->controlador->getAnswerById($resultController->Q_select,$resultController->evaluacion[2]['id_question'],$resultController->evaluacion[2]['id_selected'])) ?>
                            </option>
                        </select>
                    </div>
                    <?}else{?>
                    <div class="wrapper-danger">
                        <select selected class="select-css-danger" disabled>
                            <option>
                                <? print_r($resultController->controlador->getAnswerById($resultController->Q_select,$resultController->evaluacion[2]['id_question'],$resultController->evaluacion[2]['id_selected'])) ?>
                            </option>
                        </select>
                    </div>
                    <?}?>

                </div>
                <div class="footers">
                    <?if($resultController->evaluacion[2]['status'] !== 'true'){?>
                    <span>La respuesta correcta es: <span style="color:#76d275;font-weight: bold;">
                            <?print_r($resultController->controlador->getAnswerById($resultController->Q_select,$resultController->evaluacion[2]['id_question'], $resultController->getAnswer($resultController->Q_select,$resultController->evaluacion[2]['id_question'],'Select')))?>
                        </span></span>
                    <?}else{?>
                    <span>Puntos: 3pts.</span>
                    <?}?>
                </div>
            </div>
            <div class="questions">
                <div class="headers">
                    <? print_r($resultController->controlador->getQuestionById($resultController->Q_select,$resultController->evaluacion[3]['id_question'])) ?>
                </div>
                <div class="bodys">
                    <?if($resultController->evaluacion[3]['status']==='true'){?>
                    <div class="wrapper-success">
                        <select selected class="select-css-success" disabled>
                            <option>
                                <? print_r($resultController->controlador->getAnswerById($resultController->Q_select,$resultController->evaluacion[3]['id_question'],$resultController->evaluacion[3]['id_selected'])) ?>
                            </option>
                        </select>
                    </div>
                    <?}else{?>
                    <div class="wrapper-danger">
                        <select selected class="select-css-danger" disabled>
                            <option>
                                <? print_r($resultController->controlador->getAnswerById($resultController->Q_select,$resultController->evaluacion[3]['id_question'],$resultController->evaluacion[3]['id_selected'])) ?>
                            </option>
                        </select>
                    </div>
                    <?}?>

                </div>
                <div class="footers">
                    <?if($resultController->evaluacion[3]['status'] !== 'true'){?>
                    <span>La respuesta correcta es: <span style="color:#76d275;font-weight: bold;">
                            <?print_r($resultController->controlador->getAnswerById($resultController->Q_select,$resultController->evaluacion[3]['id_question'], $resultController->getAnswer($resultController->Q_select,$resultController->evaluacion[3]['id_question'],'Select')))?>
                        </span></span>
                    <?}else{?>
                    <span>Puntos: 3pts.</span>
                    <?}?>
                </div>
            </div>
        </div>



        <div class="block-check">
            <div class="questionc">
                <div class="headerc">
                    <? print_r($resultController->controlador->getQuestionById($resultController->Q_check,$resultController->evaluacion[4]['id_question'])) ?>
                </div>
                <div class="bodyc">
                    <?for ($i=0; $i < sizeOf($resultController->evaluacion[4]['id_selected']); $i++) { ?>
                    <?if(
                        in_array($resultController->evaluacion[4]['id_selected'][$i],$resultController->getAnswer($resultController->Q_check,$resultController->evaluacion[4]['id_question'],'Check'))
                        ){?>
                    <div class="wrapper-success">
                        <input class="" type="checkbox" checked disabled>
                        <label class="label-success" for="1">
                            <span class="text">
                                <? print_r($resultController->controlador->getAnswerById($resultController->Q_check,$resultController->evaluacion[4]['id_question'],$resultController->evaluacion[4]['id_selected'][$i])) ?>
                            </span>
                        </label>
                    </div>
                    <?}else{?>
                    <div class="wrapper-danger">
                        <input class="" type="checkbox" checked disabled>
                        <label class="label-success" for="1">
                            <span class="text">
                                <? print_r($resultController->controlador->getAnswerById($resultController->Q_check,$resultController->evaluacion[4]['id_question'],$resultController->evaluacion[4]['id_selected'][$i])) ?>
                            </span>
                        </label>
                    </div>
                    <?}?>
                    <?}?>

                </div>
                <div class="footerc">
                    <?if(
                         array_keys(array_diff($resultController->getAnswer($resultController->Q_check,$resultController->evaluacion[4]['id_question'],'Check'),$resultController->evaluacion[4]['id_selected']))
                    ){?>
                    <span>La respuesta correcta es: <span style="color:#76d275;font-weight: bold;">
                            <?print_r($resultController->controlador->getAnswerById($resultController->Q_check,
                            $resultController->evaluacion[4]['id_question'], $resultController->getAnswer($resultController->Q_check,$resultController->evaluacion[4]['id_question'],'Check')[array_keys(array_diff($resultController->getAnswer($resultController->Q_check,$resultController->evaluacion[4]['id_question'],'Check'),$resultController->evaluacion[4]['id_selected']))[0]]
                            ))?>
                        </span></span>
                    <?}?>
                    <span style="margin-left: 10px;">Puntos:
                        <?print_r($resultController->evaluacion[4]['status'])?>pts.
                    </span>

                </div>
            </div>
            <div class="questionc">
                <div class="headerc">
                    <? print_r($resultController->controlador->getQuestionById($resultController->Q_check,$resultController->evaluacion[5]['id_question'])) ?>
                </div>
                <div class="bodyc">
                    <?for ($i=0; $i < sizeOf($resultController->evaluacion[5]['id_selected']); $i++) { ?>
                    <?if(
                        in_array($resultController->evaluacion[5]['id_selected'][$i],$resultController->getAnswer($resultController->Q_check,$resultController->evaluacion[5]['id_question'],'Check'))
                        ){?>
                    <div class="wrapper-success">
                        <input class="" type="checkbox" checked disabled>
                        <label class="label-success" for="1">
                            <span class="text">
                                <? print_r($resultController->controlador->getAnswerById($resultController->Q_check,$resultController->evaluacion[5]['id_question'],$resultController->evaluacion[5]['id_selected'][$i])) ?>
                            </span>
                        </label>
                    </div>
                    <?}else{?>
                    <div class="wrapper-danger">
                        <input class="" type="checkbox" checked disabled>
                        <label class="label-success" for="1">
                            <span class="text">
                                <? print_r($resultController->controlador->getAnswerById($resultController->Q_check,$resultController->evaluacion[5]['id_question'],$resultController->evaluacion[5]['id_selected'][$i])) ?>
                            </span>
                        </label>
                    </div>
                    <?}?>
                    <?}?>

                </div>
                <div class="footerc">
                    <?if(
                         array_keys(array_diff($resultController->getAnswer($resultController->Q_check,$resultController->evaluacion[5]['id_question'],'Check'),$resultController->evaluacion[5]['id_selected']))
                    ){?>
                    <span>La respuesta correcta es: <span style="color:#76d275;font-weight: bold;">
                            <?print_r($resultController->controlador->getAnswerById($resultController->Q_check,
                            $resultController->evaluacion[5]['id_question'], $resultController->getAnswer($resultController->Q_check,$resultController->evaluacion[5]['id_question'],'Check')[array_keys(array_diff($resultController->getAnswer($resultController->Q_check,$resultController->evaluacion[5]['id_question'],'Check'),$resultController->evaluacion[5]['id_selected']))[0]]
                            ))?>
                        </span></span>
                    <?}?>
                    <span style="margin-left: 10px;">Puntos:
                        <?print_r($resultController->evaluacion[4]['status'])?>pts.
                    </span>

                </div>
            </div>
        </div>

        <div class="block-send-resp ">
            <button class="button">
                <div class="button__content">
                    <a class="button__text" style="text-decoration:none;" href="/PHP/Questions/">Reiniciar encuesta</a>
                </div>
            </button>
            <button class="button" onclick="show()">
                <div class="button__content">
                    <p class="button__text">Mis puntos</p>
                </div>
            </button>
        </div>

        <div class="bloc-result">
            <span class="text-title">Total</span>
            <div class="circulo">
                <span style="font-weight: bold; font-size: 30px;">
                    <?echo $resultController->totalPoints()?>
                </span>
                <span style="font-weight: bold; font-size: 10px;">pts</span>
            </div>
        </div>
    </div>
    <div class="block-score" id='score'>
        <div class="title-score">
            Mis puntos
        </div>
        <div class="footer-score">
            <button class="button" onclick="hide()">
                <div class="button__content">
                    <p class="button__text">Cerrar</p>
                </div>
            </button>
        </div>
    </div>
</body>
<script language='javascript' src="./script.js">
    var myPoint = <?php echo json_encode($resultController->totalPoints()); ?> + "pts"
    var date = new Date();
    var dateStr =
        ("00" + (date.getMonth() + 1)).slice(-2) + "/" +
        ("00" + date.getDate()).slice(-2) + "/" +
        date.getFullYear() + " " +
        ("00" + date.getHours()).slice(-2) + ":" +
        ("00" + date.getMinutes()).slice(-2) + ":" +
        ("00" + date.getSeconds()).slice(-2);

    if (localStorage.getItem('score')) {
        var score = []
        score.push(JSON.parse(localStorage.getItem('score')));
        score.push(
            dateStr + ' ... ' + myPoint
        )
        localStorage.setItem('score', JSON.stringify(
            score
        ))
    } else {
        localStorage.setItem('score', JSON.stringify(dateStr + ' ... ' + myPoint))
    }
</script>

</html>