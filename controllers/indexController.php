<?php
include("Controller.php");

class indexController
{
    public $radioQuestions = array();
    public $selectQuestions = array();
    public $CheckQuestions = array();
    public $controlador;

    public $values;

    public function __construct()
    {
        $this->controlador = new Controller();
        $this->radioQuestions = $this->generateQuestion($this->controlador->preguntasRadio, 2);
        $this->selectQuestions = $this->generateQuestion($this->controlador->preguntasSelect, 2);
        $this->CheckQuestions = $this->generateQuestion($this->controlador->preguntasCheck, 2);
    }

    public function generateQuestion($arrayQuestion, $quantity)
    {
        $preguntas_ram = array();
        $num_ram = range(0, sizeOf($arrayQuestion) - 1);
        shuffle($num_ram);
        $idq = array_rand($num_ram, $quantity);
        foreach ($idq as $value) {
            array_push($preguntas_ram, array_merge($arrayQuestion[$value], array_map('intval', str_split($value))));;
        }
        return $preguntas_ram;
    }


}
