<?php
include("Controller.php");

class resultController
{
    public $Q_radio;
    public $Q_select;
    public $Q_check;
    public $controlador;
    public $evaluacion = array();
    public $data_storage = array();

    public function __construct()
    {
        $this->controlador = new Controller();
        $this->Q_radio = $this->controlador->preguntasRadio;
        $this->Q_select = $this->controlador->preguntasSelect;
        $this->Q_check = $this->controlador->preguntasCheck;

        $this->evaluacion = $this->validateResults();
    }

    public static function validateInfo()
    {
        $datos = array();
        if (isset($_POST)) {
            foreach ($_POST as $value) {
                array_push($datos, $value);
            }
            return $datos;
        } else
            return null;
    } //

    public function validateResults()
    {
        $resultados_encuesta = array();
        $values = self::validateInfo();
        if (sizeOf($values) == 6) { //valido que los valores de post sean mayores oigual a 6 incluyecndo los check
            for ($i = 0; $i < sizeOf($values); $i++) {
                if ($i < 4) {
                    $array_values = explode('-', $values[$i]); // [0] => Resultado seleccionado [1] => id de pregunta

                    if ($i < 2) {
                        $Respuestas = $this->getAnswer($this->Q_radio, $array_values[1], 'Radio');
                        if ($array_values[0] == $Respuestas) {
                            array_push($resultados_encuesta, array("id_question" => $array_values[1], "id_selected" => $array_values[0], "status" => 'true'));
                        } else {
                            array_push($resultados_encuesta, array("id_question" => $array_values[1], "id_selected" => $array_values[0], "status" => 'false'));
                        }
                    } else if ($i >= 2 && $i < 4) {
                        $Respuestas = $this->getAnswer($this->Q_select, $array_values[1], 'Select');
                        if ($array_values[0] == $Respuestas) {
                            array_push($resultados_encuesta, array("id_question" => $array_values[1], "id_selected" => $array_values[0], "status" => 'true'));
                        } else {
                            array_push($resultados_encuesta, array("id_question" => $array_values[1], "id_selected" => $array_values[0], "status" => 'false'));
                        }
                    }
                } else {
                    $points = 0;
                    $array_select = array();
                    $array_select = array();
                    $id_preg = array();

                    foreach ($values[$i] as $check_value) {
                        $array_values = explode('-', $check_value); // [0] => Resultado seleccionado [1] => id de pregunta
                        $Respuestas = $this->getAnswer($this->Q_check, $array_values[1], 'Check');

                        array_push($id_preg, $array_values[1]);
                        array_push($array_select, $array_values[0]);

                        foreach ($Respuestas as $arrayCheck) {
                            if ($array_values[0] == $arrayCheck) {
                                $points++;
                            }
                        }
                    }
                    array_push($resultados_encuesta, array("id_question" => $id_preg[0], "id_selected" => $array_select, "status" => $points));
                }
            }
        }

        return $resultados_encuesta;
    } //


    public function getAnswer($arrayQuestion, $IDquestion, $tipo)
    {
        switch ($tipo) {
            case 'Radio':
                return $arrayQuestion[$IDquestion][3];
                break;

            case 'Select':
                return $arrayQuestion[$IDquestion][4];
                break;

            case 'Check':
                return $arrayQuestion[$IDquestion][5];
                break;
        }
    }

    public function totalPoints()
    {
        $total = 0;
        foreach ($this->evaluacion  as $value) {
            if ($value['status'] === 'true') {
                $total += 3;
            } else if (is_numeric($value['status'])) {
                $total += $value['status'];
                // $value['status'] = 'true'
            }
        }
        return $total;
    }
}
